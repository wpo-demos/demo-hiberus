package ec.krugercorp.hiberus.common.object;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PairTupleObject<A,B> {
    protected A valueA;
    protected B valueB;
}
