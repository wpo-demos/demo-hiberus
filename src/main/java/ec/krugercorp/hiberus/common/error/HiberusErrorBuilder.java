package ec.krugercorp.hiberus.common.error;

import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

public abstract class HiberusErrorBuilder<T> {
    private final Collection<HiberusError<T>> errors;

    protected HiberusErrorBuilder(Collection<HiberusError<T>> errors) {
        this.errors = errors;
    }

    protected HiberusErrorBuilder(int capacity, boolean withDuplicated) {
        this(withDuplicated ? new ArrayList<>(capacity) : new HashSet<>(capacity));
    }

    protected HiberusErrorBuilder(boolean withDuplicated) {
        this(10, withDuplicated);
    }

    protected HiberusErrorBuilder(int capacity) {
        this(capacity, true);
    }

    protected HiberusErrorBuilder() {
        this(5);
    }

    public Collection<HiberusError<T>> getErrors() {
        return errors;
    }

    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    public void clear() {
        errors.clear();
    }

    public abstract HiberusRuntimeException createException();

    public void build() {
        if (this.hasErrors()) {
            throw createException();
        }
    }

    protected void addMask(HiberusError<T> error) {
        errors.add(error);
    }

    public HiberusErrorBuilder<T> add(HiberusError<T> error) {
        addMask(error);
        return this;
    }

    public HiberusErrorBuilder<T> addIfTrue(boolean value, HiberusError<T> error) {
        if (value) {
            addMask(error);
        }
        return this;
    }

    public HiberusErrorBuilder<T> addIfTrue(boolean value, T error, String message, Integer index) {
        return addIfTrue(value, new HiberusError<>(error, message, index));
    }

    public HiberusErrorBuilder<T> addIfFalse(boolean expression, T error, String message) {
        return addIfTrue(!expression, error, message);
    }

    public HiberusErrorBuilder<T> addIfTrue(boolean value, T error, String message) {
        return addIfTrue(value, error, message, null);
    }

    public HiberusErrorBuilder<T> addIfNull(Object value, T error, String message) {
        return addIfTrue(value == null, error, message);
    }

    public HiberusErrorBuilder<T> addIfEmpty(String value, T error, String message) {
        return addIfTrue(value == null || value.isEmpty(), error, message);
    }

    public <R> HiberusErrorBuilder<T> addIfEmpty(Collection<R> collection, T error, String message) {
        return addIfTrue(CollectionUtils.isEmpty(collection), error, message);
    }

    public <K, V> HiberusErrorBuilder<T> addIfEmpty(Map<K, V> map, T error, String message) {
        return addIfTrue(map == null || map.isEmpty(), error, message);
    }
}