package ec.krugercorp.hiberus.domain.enums;

import lombok.Getter;

@Getter
public enum ErrorType {
    NONE,//0
    DEFAULT,//1
    UNKNOWN,//2

    //Errores mapeados por tipo de excepción
    INTERNAL_SERVER,//3
    CATCHED_SERVER,//4
    REQUEST_CONTENT_REQUIRED,//5
    REQUEST_CONTENT_INVALID,//6

    //Errores de proposito general y reutillizables
    RESOURCE_NOT_FOUND,//7

    RULE_NOT_FOUND
}
