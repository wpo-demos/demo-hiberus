package ec.krugercorp.hiberus.domain.errors;

import ec.krugercorp.hiberus.common.error.HiberusErrorBuilder;
import ec.krugercorp.hiberus.common.error.HiberusRuntimeException;
import ec.krugercorp.hiberus.domain.enums.ErrorType;

public class AppErrorBuilder extends HiberusErrorBuilder<ErrorType> {

    public AppErrorBuilder(boolean withDuplicated) {
        super(withDuplicated);
    }

    public AppErrorBuilder() {
        super();
    }

    protected AppErrorBuilder(int capacity, boolean withDuplicated) {
        super(capacity, withDuplicated);
    }

    @Override
    public HiberusRuntimeException createException() {
        return new AppException(this.getErrors());
    }

    public static AppErrorBuilder createBuilderFromRequest(Object request, boolean withDuplicated, int maxErrors) {
        var builder = new AppErrorBuilder(maxErrors, withDuplicated);
        builder.addIfNull(request, ErrorType.REQUEST_CONTENT_REQUIRED, "Los datos son obligatorios.");
        builder.build();
        return builder;
    }

    public static AppErrorBuilder createBuilderFromRequest(Object request) throws AppException {
        return createBuilderFromRequest(request, true, 10);
    }
}