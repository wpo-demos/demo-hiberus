package ec.krugercorp.hiberus.domain.dto.superheroes;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class SuperHeroCreateRequest {
    private String alias;
    private String firstName;
    private String lastName;
    private String skills;
    private boolean gender;
    private String dateOfBirth;
}
