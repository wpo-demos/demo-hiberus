package ec.krugercorp.hiberus.domain.dto.superheroes;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class SuperHeroDetail extends SuperHeroEditRequest {
    private byte status;
    private String createdDate;
    private String modifiedDate;
}
