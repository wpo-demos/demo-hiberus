package ec.krugercorp.hiberus.domain.dto.superheroes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuperHeroFilterRequest {
    private String name;
}
