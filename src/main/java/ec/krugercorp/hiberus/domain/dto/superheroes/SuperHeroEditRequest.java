package ec.krugercorp.hiberus.domain.dto.superheroes;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class SuperHeroEditRequest extends SuperHeroCreateRequest {
    private String id;
}
