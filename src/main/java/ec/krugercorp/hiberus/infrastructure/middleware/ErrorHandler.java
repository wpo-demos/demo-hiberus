package ec.krugercorp.hiberus.infrastructure.middleware;

import ec.krugercorp.hiberus.common.error.HiberusError;
import ec.krugercorp.hiberus.common.error.HiberusRuntimeException;
import ec.krugercorp.hiberus.domain.enums.ErrorType;
import ec.krugercorp.hiberus.domain.errors.AppException;
import ec.krugercorp.hiberus.infrastructure.controller.response.ErrorResponse;
import com.google.gson.JsonSyntaxException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    private static final List<HiberusError<ErrorType>> ERROR_UNKNOWN_LIST;

    static {
        ERROR_UNKNOWN_LIST = new ArrayList<>(1);
        ERROR_UNKNOWN_LIST.add(new HiberusError<>(ErrorType.UNKNOWN, AppException.DEFAULT_MESSAGE));
    }

    @ExceptionHandler(AppException.class)
    public ResponseEntity<ErrorResponse> handleHiberusAppException(
            AppException ex) {
        return ResponseEntity.badRequest()
                .body(ErrorResponse.builder().errors(ex.getErrors()).build());
    }

    @ExceptionHandler(HiberusRuntimeException.class)
    public ResponseEntity<ErrorResponse> handleHiberusRuntimeException(
            HiberusRuntimeException ex) {
        if(ex.getCause() != null) {
            log.error(ex.readAll());
        }
        var list = new ArrayList<HiberusError<ErrorType>>(1);
        list.add(new HiberusError<>(ErrorType.DEFAULT, ex.getMessage()));
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(ErrorResponse.builder().errors(list).build());
    }

    @ExceptionHandler(JsonSyntaxException.class)
    public ResponseEntity<ErrorResponse> handleException(
            JsonSyntaxException ex) {
        log.error(HiberusRuntimeException.readAll(ex));
        var listError = new ArrayList<HiberusError<ErrorType>>(1);
        listError.add(new HiberusError<>(ErrorType.REQUEST_CONTENT_INVALID, "Los datos recibidos son inválidos."));
        return ResponseEntity.badRequest()
                .body(ErrorResponse.builder().errors(listError).build());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleException(
            Exception ex) {
        log.error(HiberusRuntimeException.readAll(ex));
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ErrorResponse.builder().errors(ERROR_UNKNOWN_LIST).build());
    }
}
