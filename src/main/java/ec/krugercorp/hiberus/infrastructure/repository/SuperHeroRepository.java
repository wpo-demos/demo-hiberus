package ec.krugercorp.hiberus.infrastructure.repository;

import ec.krugercorp.hiberus.common.error.HiberusRuntimeException;
import ec.krugercorp.hiberus.domain.enums.StatusType;
import ec.krugercorp.hiberus.domain.errors.AppException;
import ec.krugercorp.hiberus.domain.repository.ISuperHeroRepository;
import ec.krugercorp.hiberus.domain.dto.superheroes.SuperHeroCreateRequest;
import ec.krugercorp.hiberus.domain.dto.superheroes.SuperHeroDetail;
import ec.krugercorp.hiberus.domain.dto.superheroes.SuperHeroEditRequest;
import ec.krugercorp.hiberus.domain.dto.superheroes.SuperHeroFilterRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.sql.ResultSet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Slf4j
@Repository
public class SuperHeroRepository implements ISuperHeroRepository {

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    private JdbcTemplate jdbcTemplate;
    private SuperHeroJPATemplate jpaTemplate;

    @Autowired
    public SuperHeroRepository(JdbcTemplate jdbcTemplate, SuperHeroJPATemplate jpaTemplate){
        this.jdbcTemplate = jdbcTemplate;
        this.jpaTemplate = jpaTemplate;
    }

    @Override
    public List<SuperHeroDetail> selectAll() {
        return jdbcTemplate.query("SELECT * FROM super_heroes WHERE status > ?",
                new Object[] { StatusType.DELETED.ordinal() }, this::createSuperHeroDetail);
    }

    @Override
    public List<SuperHeroDetail> selectByFilters(SuperHeroFilterRequest dto) {
        return jdbcTemplate.query("SELECT * FROM super_heroes WHERE status > ? AND alias LIKE ?",
                new Object[] { StatusType.DELETED.ordinal(), "%" + dto.getName() +"%" }, this::createSuperHeroDetail);
    }

    @Override
    public SuperHeroDetail selectById(String id) {
        try {
            return jdbcTemplate.queryForObject("SELECT * FROM super_heroes WHERE id = ? AND status > ?",
                    new Object[] { id, StatusType.DELETED.ordinal() }, this::createSuperHeroDetail
            );
        } catch (Exception ex) {
            throw new HiberusRuntimeException("El Recurso no a podido ser encontrado", ex);
        }
    }

    @Override
    public String insert(SuperHeroCreateRequest dto) {
        var id = UUID.randomUUID().toString();
        /*jdbcTemplate.update(
                "INSERT INTO super_heroes (id, alias, first_name, last_name, skills, gender, date_of_birth, status, created_date, modified_date, deleted_date) values(?,?,?,?,?,?,?,?,?,?,?)",
                id, dto.getAlias(), dto.getFirstName(), dto.getLastName(), dto.getSkills(),
                dto.isGender() ? "M" : "F", dto.getDateOfBirth(), StatusType.ENABLED.ordinal(),
                DATE_FORMAT.format(new Date()), null, null);*/
        jpaTemplate.save(SuperHeroModel.builder()
                .id(id)
                .alias(dto.getAlias())
                .first_name(dto.getFirstName())
                .last_name(dto.getLastName())
                .skills(dto.getSkills())
                .gender(dto.isGender() ? "M" : "F")
                .date_of_birth(dto.getDateOfBirth())
                .status(StatusType.ENABLED.ordinal())
                .created_date(DATE_FORMAT.format(new Date()))
                .modified_date(null)
                .deleted_date(null)
                .build());
        return id;
    }

    @Override
    public void update(SuperHeroEditRequest dto) {
        var data = jpaTemplate.findById(dto.getId());
        if(data.isEmpty()) {
            throw new HiberusRuntimeException("El super heroe no pudo ser encontrado");
        }
        var entity = data.get();
        entity.setAlias(dto.getAlias());
        entity.setFirst_name(dto.getFirstName());
        entity.setLast_name(dto.getLastName());
        entity.setSkills(dto.getSkills());
        entity.setGender(dto.isGender() ? "M" : "F");
        entity.setDate_of_birth(dto.getDateOfBirth());
        entity.setStatus(StatusType.ENABLED.ordinal());
        entity.setModified_date(DATE_FORMAT.format(new Date()));
        jpaTemplate.save(entity);
    }

    @Override
    public void delete(String id) {
        jdbcTemplate.update("UPDATE super_heroes SET status = ? WHERE id = ?", StatusType.DELETED.ordinal(), id);
    }

    private SuperHeroDetail createSuperHeroDetail(ResultSet rs, int rowNum) {
        try {
            return SuperHeroDetail.builder()
                    .id(rs.getString("id"))
                    .alias(rs.getString("alias"))
                    .firstName(rs.getString("first_name"))
                    .lastName(rs.getString("last_name"))
                    .skills(rs.getString("skills"))
                    .gender(rs.getString("gender").equals("M"))
                    .dateOfBirth(rs.getString("date_of_birth"))
                    .status(rs.getByte("status"))
                    .createdDate(rs.getString("created_date"))
                    .modifiedDate(rs.getString("modified_date"))
                    .build();
        } catch (Exception ex) {
            throw new HiberusRuntimeException(ex);
        }
    }
}
