package ec.krugercorp.hiberus.infrastructure.repository;

import org.springframework.data.repository.CrudRepository;

public interface SuperHeroJPATemplate extends CrudRepository<SuperHeroModel, String> {

}