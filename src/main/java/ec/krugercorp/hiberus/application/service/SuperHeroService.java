package ec.krugercorp.hiberus.application.service;

import ec.krugercorp.hiberus.domain.dto.superheroes.SuperHeroCreateRequest;
import ec.krugercorp.hiberus.domain.dto.superheroes.SuperHeroDetail;
import ec.krugercorp.hiberus.domain.dto.superheroes.SuperHeroEditRequest;
import ec.krugercorp.hiberus.domain.dto.superheroes.SuperHeroFilterRequest;
import ec.krugercorp.hiberus.infrastructure.repository.SuperHeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Component
@Service
public class SuperHeroService {
    private final SuperHeroRepository repository;

    @Autowired
    public SuperHeroService(final SuperHeroRepository repository) {
        this.repository = repository;
    }

    public List<SuperHeroDetail> getAll() {
        return repository.selectAll();
    }

    public List<SuperHeroDetail> find(SuperHeroFilterRequest dto) {
        return repository.selectByFilters(dto);
    }

    public SuperHeroDetail get(String id) {
        return repository.selectById(id);
    }

    public String create(SuperHeroCreateRequest dto) {
        return repository.insert(dto);
    }

    public void edit(SuperHeroEditRequest dto) {
        repository.update(dto);
    }

    public void delete(String id) {
        repository.delete(id);
    }
}
