DROP TABLE IF EXISTS super_heroes;

CREATE TABLE super_heroes (
    id VARCHAR(100) NOT NULL,
    alias VARCHAR(250) NOT NULL,
    first_name VARCHAR(250) NOT NULL,
    last_name VARCHAR(250) NOT NULL,
    skills VARCHAR(250) NOT NULL,
    gender VARCHAR(1) NOT NULL,
    date_of_birth VARCHAR(30) NOT NULL,
    status INT NOT NULL,
    created_date VARCHAR(30) NOT NULL,
    modified_date VARCHAR(30) DEFAULT NULL,
    deleted_date VARCHAR(30) DEFAULT NULL
);